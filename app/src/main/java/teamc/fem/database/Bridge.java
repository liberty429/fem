package teamc.fem.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import java.security.MessageDigest;
import android.database.Cursor;

/**
 * Created by j402 on 2015/07/06.
 */

//データベース操作のためのクラス
public class Bridge
{
    public static int CONTENT_TERM = 0;
    public static int CONTENT_QSN= 1;

    private SQLiteDatabase db;

    //コンストラクタ
    public Bridge( Context context )
    {
        DBHelper dbhelper = new DBHelper( context );
        db = dbhelper.getWritableDatabase();
    }

    //ユーザー作成
    public void create_user( String name, String passwd ) throws Exception
    {
        ContentValues values = new ContentValues();
        values.put( "id", name );
        values.put( "passwd", getMD5( passwd ) );
        db.insert( "user" , "", values );
    }

    //ユーザー削除
    public boolean delete_user( String name, String passwd ) throws Exception
    {
        String[] cols = {"id"};
        String selection = "id = ? && passwd = ? ";
        String[] selectionArgs = { name, passwd };
        String groupBy = null;
        String having = null;
        String orderBy = null;
        Cursor cursor = db.query("user", cols, selection, selectionArgs, groupBy, having, orderBy);
        if( cursor.getCount() > 0 )
        {
            db.delete("table_name", "id = " + name, null);
            return true;
        }

        return false;
    }

    //ログイン
    public String login( String name, String passwd ) throws Exception
    {
        String[] cols = {"id"};
        String selection = "id = ? && passwd = ? ";
        String[] selectionArgs = { name, passwd };
        String groupBy = null;
        String having = null;
        String orderBy = null;
        Cursor cursor = db.query("user", cols, selection, selectionArgs, groupBy, having, orderBy);
        if( cursor.getCount() > 0 )
        {
            return name;
        }

        return null;
    }

    //DBを閉じる
    public void close() throws Exception
    {
        db.close();
    }

    //ハッシュ値を求める
    private String getMD5( String c ) throws Exception
    {
        MessageDigest md = MessageDigest.getInstance("MD5");
        // ハッシュ値を計算
        md.update(c.getBytes());
        byte[] digest = md.digest();

        // 16進数文字列に変換
        StringBuffer buffer = new StringBuffer();
        for(int i = 0; i < digest.length; i++) {
            String tmp = Integer.toHexString(digest[i] & 0xff);
            if(tmp.length() == 1) {
                buffer.append('0').append(tmp);
            } else {
                buffer.append(tmp);
            }
        }

        return buffer.toString();
    }

    //検索
    public Cursor getQuery()
    {
        String[] cols = {"id"};
        String selection = "id = ? && passwd = ? ";
        String[] selectionArgs = {};
        String groupBy = null;
        String having = null;
        String orderBy = null;
        Cursor cursor = db.query("user", cols, selection, selectionArgs, groupBy, having, orderBy);
        if( cursor.getCount() > 0 )
        {
            return cursor;
        }

        return null;
    }
}
