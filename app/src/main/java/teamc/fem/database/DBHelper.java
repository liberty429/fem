package teamc.fem.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by j402 on 2015/07/06.
 */
public class DBHelper extends SQLiteOpenHelper
{
    private final static String DB_NAME = "fem.db";
    private final static int DB_VERSION = 1;

    //コンストラクタ
    public DBHelper( Context context)
    {
        super( context, DB_NAME, null, DB_VERSION );
    }

    //データベースの生成
    @Override
    public void onCreate( SQLiteDatabase db )
    {
        db.execSQL( "create table if not exists user    ( id text primary key, passwd blob )" );
        db.execSQL( "create table if not exists question( id text primary key, term_id text, num integer," +
                " contents text, image blob, option1 text, option2 text," +
                " option3 text, option4 text, answer text )" );
        db.execSQL( "create table if not exists term    ( id text primary key, year integer, term integer )" );
        db.execSQL( "create table if not exists history ( user_id text, question_id text, result )" );
    }

    //データベースのアップグレード
    @Override
    public void onUpgrade( SQLiteDatabase db, int aldVersion, int newVersion )
    {
        db.execSQL( "drop table if not exists user" );
        db.execSQL( "drop table if not exists question" );
        db.execSQL( "drop table if not exists term" );
        db.execSQL( "drop table if not exists history" );
        onCreate( db );
    }

    private void setdefault()
    {

    }

}
