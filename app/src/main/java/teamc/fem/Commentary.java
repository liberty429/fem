package teamc.fem;

import android.app.Activity;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import teamc.fem.R;

/**
 * Created by J411 on 2015/07/13.
 */
public class Commentary extends Activity {

        int index = 0;
        String[] comment = new String[80];//解説を1～80ページ分格納する配列
        String[] answer  = new String[80];//解答を〃

        Button nextPage = (Button) findViewById(R.id.nextPage); //”次の問題へ”ボタン
        Button prevPage = (Button) findViewById(R.id.prevPage); //”前の問題へ”ボタン
        Button topPage  = (Button) findViewById(R.id.examPage); //"問題画面に戻る"ボタン
        Button homePage = (Button) findViewById(R.id.topPage);  //"Top画面へ"ボタン
        Button goPage = (Button) findViewById(R.id.goPage);      //"Go"ボタン

        EditText changePage = (EditText)findViewById(R.id.changePage); //～ページに切り替え用入力フィールド

        CheckBox checkExam = (CheckBox)findViewById(R.id.checkExam);   //"後で見る"チェックボックス

        TextView title      = (TextView)findViewById(R.id.title);              //タイトルテキストフィールド
        TextView cmntInfo   = (TextView)findViewById(R.id.commentInfo);  //解説記述テキストフィールド
        TextView answerInfo = (TextView)findViewById(R.id.answerInfo);   //解答記述テキストフィールド

        ImageView cmntIMG = (ImageView)findViewById(R.id.commentImage);  //解説図イメージ
}
