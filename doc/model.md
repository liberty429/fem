﻿# ユーザ
	* ユーザID
	* パスワード
	
# 問題
	* 問題ID
	* 年期ID
	* 番号
	* 問題文
	* 画像
	* 選択肢ア
	* 選択肢イ
	* 選択肢ウ
	* 選択肢エ
	* 解答

# 年期
	* 年期ID
	* 年度
	* 期
	
# 履歴
	* ユーザID
	* 問題ID
	* 結果